**A systematic approach to bacterial phylogeny via proteome clustering of diverse orders with identification of high-HGT species using network science**

There are three text documents available. 


1- Cluster-Membership-MainDataset.txt

2- Cluster-Membership-FirstExtraDataset.txt

3- Cluster-Membership-SecondExtraDataset.txt

Each file contains a list of clusters. The first line shows number of sequences (N) in each cluster, and the cluster number. The next N lines represent the sequences. 
Here is an example:


4 Sequences: Cluster: 10
0 >AAQ57681.1 chromosomal replication iniciator protein DnaA [Chromobacterium violaceum ATCC 12472]
5516 >AAP77707.1 chromosomal replication initiator protein DnaA [Helicobacter hepaticus ATCC 51449]
9193 >BAG41031.1 chromosomal replication initiator protein DnaA [Orientia tsutsugamushi str. Ikeda]
9588 >BAH37043.1 chromosomal replication initiator protein DnaA [Gemmatimonas aurantiaca T-27]